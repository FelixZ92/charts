prometheus-operator:
  defaultRules:
    create: true
    rules:
      alertmanager: true
      etcd: true
      general: true
      k8s: true
      kubeApiserver: true
      kubeApiserverAvailability: true
      kubeApiserverError: true
      kubeApiserverSlos: true
      kubelet: true
      kubePrometheusGeneral: true
      kubePrometheusNodeAlerting: true
      kubePrometheusNodeRecording: true
      kubernetesAbsent: true
      kubernetesApps: true
      kubernetesResources: true
      kubernetesStorage: true
      kubernetesSystem: true
      kubeScheduler: true
      kubeStateMetrics: true
      network: true
      node: true
      prometheus: true
      prometheusOperator: true
      time: true

  global:
    rbac:
      create: true
      pspEnabled: true

  alertmanager:
    enabled: true
    podDisruptionBudget:
      enabled: true
      minAvailable: 1
      maxUnavailable: ""
    config:
      global:
        resolve_timeout: 5m
      route:
        group_by: ['job']
        group_wait: 30s
        group_interval: 5m
        repeat_interval: 12h
        receiver: 'slack'
        routes:
          - match:
              alertname: Watchdog
            receiver: 'null'
      receivers:
        - name: 'null'
        - name: 'slack'
          slack_configs:
            - api_url: https://hooks.slack.com/services/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
              channel: '#alerts'
              send_resolved: true

    ingress:
      enabled: false

    alertmanagerSpec:
      image:
        repository: quay.io/prometheus/alertmanager
        tag: v0.21.0
      replicas: 2
      retention: 120h
      storage:
        volumeClaimTemplate:
          spec:
            storageClassName: rook-ceph-block
            accessModes: ["ReadWriteOnce"]
            resources:
              requests:
                storage: 5400Mi
      externalUrl: https://alertmanager
      routePrefix: /
      nodeSelector:
        storage: local

      resources:
        requests:
          memory: "200Mi"
          cpu: "100m"
        limits:
          memory: "300Mi"
          cpu: "200m"
      priorityClassName: ""


  ## Using default values from https://github.com/helm/charts/blob/master/stable/grafana/values.yaml
  ##
  grafana:
    enabled: true
    image:
      tag: 7.2.0-beta2
    ## Deploy default dashboards.
    ##
    defaultDashboardsEnabled: true

    # Created and sealed from passwordstore via 'make create-grafana-secret'
    admin:
      existingSecret: "grafana-admin-user"
      userKey: username
      passwordKey: password

    envFromSecret: "grafana-generic-oauth"

    plugins:
      - grafana-piechart-panel
      - grafana-clock-panel
    dashboards:
      default:
        ceph-cluster:
          gnetId: 2842
          revision: 14
          datasource: Prometheus
        ceph-ods:
          gnetId: 5336
          revision: 5
          datasource: Prometheus
        ceph-pools:
          gnetId: 5342
          revision: 5
          datasource: Prometheus

    sidecar:
      dashboards:
        searchNamespace: "ALL"

    extraConfigmapMounts: []
    # - name: certs-configmap
    #   mountPath: /etc/grafana/ssl/
    #   configMap: certs-configmap
    #   readOnly: true

    # TODO use this for Loki
    ## Configure additional grafana datasources (passed through tpl)
    ## ref: http://docs.grafana.org/administration/provisioning/#datasources
    additionalDataSources:
      - name: Loki
        type: loki
        access: proxy
        url: http://loki.loki.svc:3100
        version: 1
    # - name: prometheus-sample
    #   access: proxy
    #   basicAuth: true
    #   basicAuthPassword: pass
    #   basicAuthUser: daco
    #   editable: false
    #   jsonData:
    #       tlsSkipVerify: true
    #   orgId: 1
    #   type: prometheus
    #   url: https://{{ printf "%s-prometheus.svc" .Release.Name }}:9090
    #   version: 1


  ## Component scraping the kube controller manager
  ##
  kubeControllerManager:
    enabled: true
    endpoints:
      - 192.168.0.11
    # - 10.141.4.23
    # - 10.141.4.24

  ## Component scraping coreDns. Use either this or kubeDns
  ##
  coreDns:
    enabled: true

  kubeEtcd:
    enabled: true
    endpoints:
      - 192.168.0.11
    # - 10.141.4.23
    # - 10.141.4.24
    service:
      port: 2381
      targetPort: 2381

  kubeScheduler:
    enabled: true
    endpoints:
      - 192.168.0.11
    # - 10.141.4.23
    # - 10.141.4.24

  kubeProxy:
    enabled: false
    endpoints:
      - 192.168.0.11
      - 192.168.0.12
    # - 10.141.4.23
    # - 10.141.4.24

  ## Manages Prometheus and Alertmanager components
  ##
  prometheusOperator:
    enabled: true

    # If true prometheus operator will create and update its CRDs on startup
    # Only for prometheusOperator.image.tag < v0.39.0
    manageCrds: true

    tlsProxy:
      enabled: true
      image:
        repository: squareup/ghostunnel
        tag: v1.5.2
      resources: {}

    admissionWebhooks:
      patch:
        enabled: true
        image:
          repository: jettech/kube-webhook-certgen
          tag: v1.2.1
        resources: {}

    image:
      repository: quay.io/coreos/prometheus-operator
      tag: v0.38.1

    ## Configmap-reload image to use for reloading configmaps
    ##
    configmapReloadImage:
      repository: docker.io/jimmidyson/configmap-reload
      tag: v0.3.0

    ## Prometheus-config-reloader image to use for config and rule reloading
    ##
    prometheusConfigReloaderImage:
      repository: quay.io/coreos/prometheus-config-reloader
      tag: v0.38.1

    hyperkubeImage:
      repository: k8s.gcr.io/hyperkube
      tag: v1.16.12

  ## Deploy a Prometheus instance
  ##
  prometheus:
    podDisruptionBudget:
      enabled: true
      minAvailable: 1
      maxUnavailable: ""

    prometheusSpec:
      image:
        repository: quay.io/prometheus/prometheus
        tag: v2.18.2
      externalUrl: "https://prometheus"

      nodeSelector:
        storage: local
      retention: 10d
      retentionSize: ""
      replicas: 2

      storageSpec:
        volumeClaimTemplate:
          spec:
            storageClassName: rook-ceph-block
            accessModes: ["ReadWriteOnce"]
            resources:
              requests:
                storage: 5400Mi
      priorityClassName: ""
      secrets:
        - kubedb-apiserver-cert

  prometheus-node-exporter:
    resources:
      limits:
        cpu: 200m
        memory: 50Mi
      requests:
        cpu: 100m
        memory: 30Mi
