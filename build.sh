#!/usr/bin/env sh
set -e

helm version
helm repo add elastic https://helm.elastic.co
helm repo add felixz https://felixz92.gitlab.io/charts
helm repo add loki https://grafana.github.io/loki/charts
helm repo add codecentric https://codecentric.github.io/helm-charts
for chart in $(find . -maxdepth 1 -type d -not -path '.' -not -path '*/\.*' -not -path '\./public' -not -path '\./target' -not -path '\./venv' -not -path '\./backup'); do
  echo "Update deps for chart ${chart}"
  helm dependency update ${chart}
  helm lint "${chart}"
done

mkdir -p ./public
printf "User-Agent: *\nDisallow: /" > ./public/robots.txt
helm package $(find . -maxdepth 1 -type d -not -path '.' -not -path '*/\.*' -not -path '\./public' -not -path '\./target' -not -path '\./venv' -not -path '\./backup' | xargs echo) --destination ./public
helm repo index --url https://${CI_PROJECT_NAMESPACE}.gitlab.io/${CI_PROJECT_NAME} .
sed -i -e 's/public\///g' index.yaml
mv index.yaml ./public

ls -la public/
