apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "gatekeeper.fullname" . }}
  labels:
    {{- include "gatekeeper.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "gatekeeper.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "gatekeeper.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "gatekeeper.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      volumes:
        - name: config
          configMap:
            name: {{ include "gatekeeper.fullname" . }}-rules
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          volumeMounts:
            - mountPath: /etc/gatekeeper
              name: config
          args:
            - --enable-default-deny=false
            - --listen=:3000
            - --listen-admin=:9000
            - --listen-admin-scheme=http
            - --encryption-key=$(ENCRYPTION_KEY)
            - --client-id=$(CLIENT_ID)
            - --client-secret=$(CLIENT_SECRET)
            - --config=/etc/gatekeeper/config.yaml
            - --redirection-url=$(REDIRECTION_URL)
            - --upstream-url=$(UPSTREAM_URL)
            - --discovery-url=$(DISCOVERY_URL)
            - --skip-openid-provider-tls-verify=$(SKIP_OPENID_PROVIDER_TLS_VERIFY)
            - --enable-metrics
            - --skip-token-verification=$(SKIP_TOKEN_VERIFICATION)
          env:
            - name: REDIRECTION_URL
              value: {{ .Values.config.redirectionUrl }}
            - name: UPSTREAM_URL
              value: {{ .Values.config.upstreamUrl }}
            - name: DISCOVERY_URL
              value: {{ .Values.config.discoveryUrl }}
            - name: SKIP_OPENID_PROVIDER_TLS_VERIFY
              value: {{ .Values.config.skipOpenidProviderTlsVerify | quote }}
            - name: SKIP_TOKEN_VERIFICATION
              value: {{ .Values.config.skipTokenVerification | quote }}
            - name: CLIENT_ID
              valueFrom:
                secretKeyRef:
                  key: {{ .Values.config.client.idKey }}
                  name: {{ .Values.config.client.secretName }}
            - name: CLIENT_SECRET
              valueFrom:
                secretKeyRef:
                  key: {{ .Values.config.client.secretKey }}
                  name: {{ .Values.config.client.secretName }}
            - name: ENCRYPTION_KEY
              valueFrom:
                secretKeyRef:
                  key: {{ .Values.config.encryptionKey.secretKey }}
                  name: {{ .Values.config.encryptionKey.secretName }}

          ports:
            - name: http
              containerPort: 3000
              protocol: TCP
            - name: admin
              containerPort: 9000
              protocol: TCP
          livenessProbe:
            httpGet:
              port: admin
              path: /oauth/health
          readinessProbe:
            httpGet:
              port: admin
              path: /oauth/health
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
